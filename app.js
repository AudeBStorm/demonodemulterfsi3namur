const express = require('express');
const mongoose = require('mongoose');
require('express-async-errors');

/// Variable d'env en dur pour la demo (à mettre dans vos fichiers d'env)
const PORT = 8080;
const NODE_ENV = 'development'
const DB_CONNECT = 'mongodb://localhost/DemoMulter'

//Création de l'app express
const app = express();

//Middleware pour utiliser json en post
app.use(express.json())
//Middleware pour pouvoir utiliser un dossier public dans notre server backend
app.use(express.static('public'));

//BDD connect
app.use(async(req, res, next) => {
    await mongoose.connect(DB_CONNECT);
    console.log('Connection réussie !');
    next();
})

//Routing
app.use('/api', require('./routes'));

//Start
app.listen(PORT, () => {
    console.log(`Server lancé sur le port ${PORT} [${NODE_ENV}]`);
})