const User = require("../models/user-model");

const userController = {
    getAll : async (req, res) => {
        const users = await User.find();
        res.status(200).json(users);
    },
    create : async (req, res) => {
        //ATTENTION !!!!
        //Quand on travaille avec des fichiers en post, on devra toujours le mettre en multipart form data

        const { lastname, firstname } = req.body;

        //Dans req.file on récupère le fichier qui a été fourni par l'utilisateur
        console.log('fichier après insertion backend', req.file);

        const userToInsert = User({
            lastname,
            firstname,
            //Dans req.file.filename se trouve le nom du fichier après insertion dans le serveur
            avatar : `http://localhost:8080/avatars/${req.file.filename}`
            // avatar : `http://localhost:${PORT}/avatars/${req.file.filename}` -> plus clean : récupérer le port dans vos variables d'env

        });

        await userToInsert.save();
        res.status(200).json(userToInsert)
    }
}

module.exports = userController;