const { Schema, model } = require("mongoose");

const userSchema = new Schema({
    lastname : {
        type : String,
        required : true
    },
    firstname : {
        type : String,
        required : true
    },
    avatar : {
        type : String
    }
}, {
    collection : 'User'
})

const User = model('User', userSchema);
module.exports = User;