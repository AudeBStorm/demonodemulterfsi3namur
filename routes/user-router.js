const userController = require('../controllers/user-controller');

const userRouter = require('express').Router();

//On importe la librairie multer
const multer = require("multer");
//On importe la librairie uuid -> permet de générer un id aléatoire sur 16 octets en hexa
const uuid = require('uuid');

//Paramétrage de multer
// - Basique :
// const upload = multer({ dest : 'public/avatars'});

// - Custom :
const storage = multer.diskStorage({
    //prop pour paramétrer la destination de l'image
    destination : (req, file, cb) => { cb(null, 'public/avatars')},
    //prop pour paramétrer le nom qu'aura notre image sauvegardée
    filename : (req, file, cb) => {
        // console.log('infos file : ', file);
        //On va générer un nom de fichier aléatoire :
        const name = uuid.v4();
        //Récupérer l'extension du fichier qu'on veut ajouter
        //pp2.pouet.png -> après split [pp2, pouet, png] -> on veut récup le dernier élément
        //at(indice) -> nouvelle fonctionnalité ecma -> permet de récupérer un élément à l'indice fourni en paramètre
        //at(-1) -> récupère toujours le dernier élément (-2, l'avant dernier etc)
        const extension = file.originalname.split('.').at(-1);
        cb(null, name + '.' + extension)
    }
});
const upload = multer({storage});


userRouter.route('/')
    .get(userController.getAll)
    .post(upload.single('avatar'), userController.create)


module.exports = userRouter;